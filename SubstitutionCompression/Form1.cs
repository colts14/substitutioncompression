﻿/*********************************************************
 *
 * Substitution Compression
 * 
 * August 17, 2016
 * 
 * Authors: Ziauddin Ahmed, Timothy Hitchcock, Jeffrey Li,
 *          Tharmitha Manoharan, Ronald Polintan
 * 
 *  
 *********************************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;


namespace SubstitutionCompression
{
    public partial class Form1 : Form
    {
        private String directoryName = String.Empty;
        private String fileName = String.Empty;
        private Dictionary<String, String> Ascii;
        private Dictionary<String, Int32> Occurrence;
        private Dictionary<String, Double> Frequency;
        private Dictionary<String, String> Huffman_Code;
        private Dictionary<String, String> Huffman_Decoder;

        public Form1()
        {
            InitializeComponent();
            SetupColumnHeaders();
            GenerateDictionaries();
        }
        //
        // Generate ascii and occurrence dictionaries
        //
        private void GenerateDictionaries()
        {
            Ascii = new Dictionary<String, String>();
            Occurrence = new Dictionary<String, Int32>();
 
            Ascii.Add("space", "00100000");
            Ascii.Add("a", "01100001");
            Ascii.Add("b", "01100010");
            Ascii.Add("c", "01100011");
            Ascii.Add("d", "01100100");
            Ascii.Add("e", "01100101");
            Ascii.Add("f", "01100110");
            Ascii.Add("g", "01100111");
            Ascii.Add("h", "01101000");
            Ascii.Add("i", "01101001");
            Ascii.Add("j", "01101010");
            Ascii.Add("k", "01101011");
            Ascii.Add("l", "01101100");
            Ascii.Add("m", "01101101");
            Ascii.Add("n", "01101110");
            Ascii.Add("o", "01101111");
            Ascii.Add("p", "01110000");
            Ascii.Add("q", "01110001");
            Ascii.Add("r", "01110010");
            Ascii.Add("s", "01110011");
            Ascii.Add("t", "01110100");
            Ascii.Add("u", "01110101");
            Ascii.Add("v", "01110110");
            Ascii.Add("w", "01110111");
            Ascii.Add("x", "01111000");
            Ascii.Add("y", "01111001");
            Ascii.Add("z", "01111010");

            List<String> keys = new List<String>(Ascii.Keys);
            foreach (String key in keys)
            {
                Occurrence.Add(key, 0);
            }
        }
        //
        // Set all values of occurrence dictionary to zero
        //
        private void ResetOccurrenceDictionary()
        {
            List<String> keys = new List<String>(Occurrence.Keys);
            foreach(String key in keys)
            {
                 Occurrence[key] = 0;
            }
        }
        //
        // Event handler for browse button click
        //
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            // Display the openFile dialog.
            DialogResult result = openFileDialog1.ShowDialog();

            if (result.Equals(DialogResult.OK)) // if user clicks 'OK'
            {
                ClearTextBoxes();
                txtFileSelected.Text = openFileDialog1.FileName;
                directoryName = Path.GetDirectoryName(txtFileSelected.Text); // get path to directory of file
                fileName = Path.GetFileNameWithoutExtension(txtFileSelected.Text); // get name of file
                Double totalOccurrences = 0;
                ResetOccurrenceDictionary();

                try
                {
                    using (StreamReader sr = new StreamReader(txtFileSelected.Text))
                    {
                        while (sr.Peek() >= 0) // Check for end of file
                        {
                            // read each character in the file and increment appropriate dictionary value 
                            // associated with the read character
                            switch (Char.ToLower((char)sr.Read()))
                            {
                                case 'a':
                                    Occurrence["a"]++;
                                    break;
                                case 'b':
                                    Occurrence["b"]++;
                                    break;
                                case 'c':
                                    Occurrence["c"]++;
                                    break;
                                case 'd':
                                    Occurrence["d"]++;
                                    break;
                                case 'e':
                                    Occurrence["e"]++;
                                    break;
                                case 'f':
                                    Occurrence["f"]++;
                                    break;
                                case 'g':
                                    Occurrence["g"]++;
                                    break;
                                case 'h':
                                    Occurrence["h"]++;
                                    break;
                                case 'i':
                                    Occurrence["i"]++;
                                    break;
                                case 'j':
                                    Occurrence["j"]++;
                                    break;
                                case 'k':
                                    Occurrence["k"]++;
                                    break;
                                case 'l':
                                    Occurrence["l"]++;
                                    break;
                                case 'm':
                                    Occurrence["m"]++;
                                    break;
                                case 'n':
                                    Occurrence["n"]++;
                                    break;
                                case 'o':
                                    Occurrence["o"]++;
                                    break;
                                case 'p':
                                    Occurrence["p"]++;
                                    break;
                                case 'q':
                                    Occurrence["q"]++;
                                    break;
                                case 'r':
                                    Occurrence["r"]++;
                                    break;
                                case 's':
                                    Occurrence["s"]++;
                                    break;
                                case 't':
                                    Occurrence["t"]++;
                                    break;
                                case 'u':
                                    Occurrence["u"]++;
                                    break;
                                case 'v':
                                    Occurrence["v"]++;
                                    break;
                                case 'w':
                                    Occurrence["w"]++;
                                    break;
                                case 'x':
                                    Occurrence["x"]++;
                                    break;
                                case 'y':
                                    Occurrence["y"]++;
                                    break;
                                case 'z':
                                    Occurrence["z"]++;
                                    break;
                                case ' ':
                                    Occurrence["space"]++;
                                    break;
                                case ',':
                                    Occurrence["space"]++;
                                    break;
                                case ':':
                                    Occurrence["space"]++;
                                    break;
                                case ';':
                                    Occurrence["space"]++;
                                    break;
                                default:
                                    break;
                            }
                        }
                        sr.Close();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error occurred while attempting to load the file. The error is:"
                                    + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine);
                    return; // exit the method
                }

                // Create frequency dictionary
                Frequency = new Dictionary<String, Double>();
                List<String> keys = new List<String>(Occurrence.Keys);

                foreach (String key in keys)
                {
                    totalOccurrences += Occurrence[key]; // sum up the total occurrences
                }
                if (totalOccurrences > 0)
                {
                    foreach (String key in keys)
                    {
                        Frequency.Add(key, Occurrence[key] / totalOccurrences);
                    }

                    // Create Ordered_Frequency in descending order using LINQ
                    var Ordered_Frequency = (from item in Frequency
                                             orderby item.Value descending
                                             select item);

                    // Create list of Ordered_Frequency keys
                    List<String> orderedKeys = new List<String>();
                    foreach (var item in Ordered_Frequency)
                    {
                        orderedKeys.Add(item.Key);
                    }

                    // Create list of Huffman codes
                    List<String> huffmanCodes = GenerateCodeList();

                    // Create Huffman_Code and decoder dictionaries using the 2 lists
                    Huffman_Code = new Dictionary<String, String>();
                    Huffman_Decoder = new Dictionary<String, String>();

                    for (int i = 0; i < orderedKeys.Count; i++)
                    {
                        Huffman_Code.Add(orderedKeys[i], huffmanCodes[i]);
                        Huffman_Decoder.Add(huffmanCodes[i], orderedKeys[i]);
                    }

                    // Change the dictionary value "space" to " " in the decoder dictionary

                    List<String> decoderKeys = new List<String>(Huffman_Decoder.Keys);
                    foreach(String key in decoderKeys)
                    {
                        if (Huffman_Decoder[key].Equals("space"))
                            Huffman_Decoder[key] = " ";
                    }
                }
                DisplayInfo();
            }

            // Cancel button was pressed
            else if (result.Equals(DialogResult.Cancel))
            {
                return;
            }
        }
        //
        // Generate list of Huffman codes and return the list
        //
        private static List<string> GenerateCodeList()
        {
            List<String> codes = new List<String>();
            codes.Add("100");
            codes.Add("0010");
            codes.Add("0011");
            codes.Add("1111");
            codes.Add("1110");
            codes.Add("1100");
            codes.Add("1011");
            codes.Add("1010");
            codes.Add("0110");
            codes.Add("0101");
            codes.Add("11011");
            codes.Add("01111");
            codes.Add("01001");
            codes.Add("01000");
            codes.Add("00011");
            codes.Add("00010");
            codes.Add("00001");
            codes.Add("00000");
            codes.Add("110101");
            codes.Add("011101");
            codes.Add("011100");
            codes.Add("1101001");
            codes.Add("110100011");
            codes.Add("110100001");
            codes.Add("110100000");
            codes.Add("1101000101");
            codes.Add("11010001000");
            return codes;
        }
        //
        // Event handler for encrypt button click
        //
        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            String cipheredFile = directoryName + @"\" + fileName + "_Huffman_ciphered";
            Double sizeCleartext = 0;
            Double sizeCipheredText = 0;

            if (String.IsNullOrEmpty(txtFileSelected.Text)) return; // exit method if no file has been selected

            if (File.Exists(cipheredFile))
            {
                try
                {
                    File.Delete(cipheredFile); // delete the file if it exists
                } catch (IOException ex) {
                    MessageBox.Show("An error occurred while attempting to delete the file. The error is:"
                                + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine);
                return;
                }
            }
            
            // Read the original file and for each character read,
            // write the corresponding Huffman code to the encoded file.
            // Get the length of corresponding Huffman code and summate them
            try
            {
                using (StreamReader sr = new StreamReader(txtFileSelected.Text))
                {
                    using (StreamWriter sw = new StreamWriter(cipheredFile))
                    {
                        while (sr.Peek() >= 0)
                        {
                            switch (Char.ToLower((Char)sr.Read()))
                            {
                                case 'a':
                                    sw.Write(Huffman_Code["a"]);
                                    sizeCipheredText += Huffman_Code["a"].Length;
                                    break;
                                case 'b':
                                    sw.Write(Huffman_Code["b"]);
                                    sizeCipheredText += Huffman_Code["b"].Length;
                                    break;
                                case 'c':
                                    sw.Write(Huffman_Code["c"]);
                                    sizeCipheredText += Huffman_Code["c"].Length;
                                    break;
                                case 'd':
                                    sw.Write(Huffman_Code["d"]);
                                    sizeCipheredText += Huffman_Code["d"].Length;
                                    break;
                                case 'e':
                                    sw.Write(Huffman_Code["e"]);
                                    sizeCipheredText += Huffman_Code["e"].Length;
                                    break;
                                case 'f':
                                    sw.Write(Huffman_Code["f"]);
                                    sizeCipheredText += Huffman_Code["f"].Length;
                                    break;
                                case 'g':
                                    sw.Write(Huffman_Code["g"]);
                                    sizeCipheredText += Huffman_Code["g"].Length;
                                    break;
                                case 'h':
                                    sw.Write(Huffman_Code["h"]);
                                    sizeCipheredText += Huffman_Code["h"].Length;
                                    break;
                                case 'i':
                                    sw.Write(Huffman_Code["i"]);
                                    sizeCipheredText += Huffman_Code["i"].Length;
                                    break;
                                case 'j':
                                    sw.Write(Huffman_Code["j"]);
                                    sizeCipheredText += Huffman_Code["j"].Length;
                                    break;
                                case 'k':
                                    sw.Write(Huffman_Code["k"]);
                                    sizeCipheredText += Huffman_Code["k"].Length;
                                    break;
                                case 'l':
                                    sw.Write(Huffman_Code["l"]);
                                    sizeCipheredText += Huffman_Code["l"].Length;
                                    break;
                                case 'm':
                                    sw.Write(Huffman_Code["m"]);
                                    sizeCipheredText += Huffman_Code["m"].Length;
                                    break;
                                case 'n':
                                    sw.Write(Huffman_Code["n"]);
                                    sizeCipheredText += Huffman_Code["n"].Length;
                                    break;
                                case 'o':
                                    sw.Write(Huffman_Code["o"]);
                                    sizeCipheredText += Huffman_Code["o"].Length;
                                    break;
                                case 'p':
                                    sw.Write(Huffman_Code["p"]);
                                    sizeCipheredText += Huffman_Code["p"].Length;
                                    break;
                                case 'q':
                                    sw.Write(Huffman_Code["q"]);
                                    sizeCipheredText += Huffman_Code["q"].Length;
                                    break;
                                case 'r':
                                    sw.Write(Huffman_Code["r"]);
                                    sizeCipheredText += Huffman_Code["r"].Length;
                                    break;
                                case 's':
                                    sw.Write(Huffman_Code["s"]);
                                    sizeCipheredText += Huffman_Code["s"].Length;
                                    break;
                                case 't':
                                    sw.Write(Huffman_Code["t"]);
                                    sizeCipheredText += Huffman_Code["t"].Length;
                                    break;
                                case 'u':
                                    sw.Write(Huffman_Code["u"]);
                                    sizeCipheredText += Huffman_Code["u"].Length;
                                    break;
                                case 'v':
                                    sw.Write(Huffman_Code["v"]);
                                    sizeCipheredText += Huffman_Code["v"].Length;
                                    break;
                                case 'w':
                                    sw.Write(Huffman_Code["w"]);
                                    sizeCipheredText += Huffman_Code["w"].Length;
                                    break;
                                case 'x':
                                    sw.Write(Huffman_Code["x"]);
                                    sizeCipheredText += Huffman_Code["x"].Length;
                                    break;
                                case 'y':
                                    sw.Write(Huffman_Code["y"]);
                                    sizeCipheredText += Huffman_Code["y"].Length;
                                    break;
                                case 'z':
                                    sw.Write(Huffman_Code["z"]);
                                    sizeCipheredText += Huffman_Code["z"].Length;
                                    break;
                                case ' ':
                                    sw.Write(Huffman_Code["space"]);
                                    sizeCipheredText += Huffman_Code["space"].Length;
                                    break;
                                case ',':
                                    sw.Write(Huffman_Code["space"]);
                                    sizeCipheredText += Huffman_Code["space"].Length;
                                    break;
                                case ':':
                                    sw.Write(Huffman_Code["space"]);
                                    sizeCipheredText += Huffman_Code["space"].Length;
                                    break;
                                case ';':
                                    sw.Write(Huffman_Code["space"]);
                                    sizeCipheredText += Huffman_Code["space"].Length;
                                    break;
                                default:
                                    sizeCleartext -= 8; // for those characters ignored negate 8 (bits for size of ascii)
                                    break;
                            }
                            // Increment size of clear text variable
                            sizeCleartext += 8; // for each character read, add 8 (bits for size of ascii)
                        }
                        sw.Close();
                    }
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while attempting to load the file. The error is:"
                                + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine);
                return;
            }
            txtSizeCipheredText.Text = String.Format("{0:n0}", sizeCipheredText);
            txtSizeClearText.Text = String.Format("{0:n0}", sizeCleartext);
            txtCompressionRatio.Text = String.Format("{0:n} %", 100 - sizeCipheredText / sizeCleartext * 100);

            OpenFile(cipheredFile);
        }
        //
        // Eventhandler for decrypt button click
        //
        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            String cipheredFile = directoryName + @"\" + fileName + "_Huffman_ciphered";
            String decodedFile = directoryName + @"\" + fileName + "_Huffman_decoded";

            if (String.IsNullOrEmpty(txtFileSelected.Text)) return; // exit method if no file has been selected

            if (File.Exists(decodedFile))
            {
                try
                {
                    File.Delete(decodedFile); // delete decoded file if it exists
                }
                catch (IOException ex)
                {
                    MessageBox.Show("An error occurred while attempting to delete the file. The error is:"
                                + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine);
                    return;
                }
            }
            // Read each 1 or 0 from the ciphered file and continue to concatenate it to a string
            // until the sequence of 1's and 0's match a key in the Huffman decoder dictionary.
            // If a match is found, write the corresponding dictionary value onto the decoded file
            try
            {
                String code = String.Empty;
                using (StreamReader sr = new StreamReader(cipheredFile))
                {
                    using (StreamWriter sw = new StreamWriter(decodedFile))
                    {
                        while (sr.Peek() >= 0)
                        {
                            code += (Char)sr.Read();
                            String character = String.Empty;

                            if (Huffman_Decoder.TryGetValue(code, out character))
                            {
                                sw.Write(character.ToUpper());
                                code = String.Empty;
                            }
                        }
                        sw.Close();
                    }
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occurred while attempting to load the file. The error is:"
                                + System.Environment.NewLine + ex.ToString() + System.Environment.NewLine);
                return;
            }
            OpenFile(decodedFile);
        }
        //
        // Open file in notepad
        //
        private void OpenFile(string filePath)
        {
            System.Diagnostics.Process.Start("notepad.exe", filePath);
        }
        //
        // Set up column headers for listview control
        //
        private void SetupColumnHeaders()
        {
            // set up listview parameters
            lsvDisplay.View = View.Details;
            lsvDisplay.FullRowSelect = true;
            lsvDisplay.GridLines = true;

            // set up column headers
            ColumnHeader colHead;
            colHead = new ColumnHeader();
            colHead.TextAlign = HorizontalAlignment.Center;
            colHead.Width = 70;
            colHead.Text = "Letter";
            lsvDisplay.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.TextAlign = HorizontalAlignment.Center;
            colHead.Width = 80;
            colHead.Text = "Ascii";
            lsvDisplay.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.TextAlign = HorizontalAlignment.Center;
            colHead.Width = 100;
            colHead.Text = "Occurrence";
            lsvDisplay.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.TextAlign = HorizontalAlignment.Center;
            colHead.Width = 120;
            colHead.Text = "Frequency";
            lsvDisplay.Columns.Add(colHead);

            colHead = new ColumnHeader();
            colHead.TextAlign = HorizontalAlignment.Center;
            colHead.Width = 100;
            colHead.Text = "Huffman Code";
            lsvDisplay.Columns.Add(colHead);
        }
        //
        // Display info into listview control
        //
        private void DisplayInfo()
        {
            // Clear the listview
            lsvDisplay.Items.Clear();
            ListViewItem lvi = new ListViewItem();
            ListViewItem.ListViewSubItem lvsi;
            int counter = 0;

            lsvDisplay.BeginUpdate();

            foreach (KeyValuePair<string, string> entry in Ascii)
            {
                lvi = new ListViewItem();
                lvi.Text = entry.Key;

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = entry.Value;
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = String.Format("{0:n0}", Occurrence[entry.Key]);
                lvi.SubItems.Add(lvsi);


                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = String.Format("{0:n} %", Frequency[entry.Key] * 100);
                lvi.SubItems.Add(lvsi);

                lvsi = new ListViewItem.ListViewSubItem();
                lvsi.Text = Huffman_Code[entry.Key];
                lvi.SubItems.Add(lvsi);

                lsvDisplay.Items.Add(lvi);

                if ((counter % 2).Equals(0))
                {
                    lsvDisplay.Items[counter].BackColor = Color.FromArgb(235, 235, 235);
                }
                counter++;
            }
            lsvDisplay.EndUpdate();
        }
        //
        // Clear compression ratio textboxes
        //
        private void ClearTextBoxes()
        {
            txtSizeCipheredText.Text = String.Empty;
            txtSizeClearText.Text = String.Empty;
            txtCompressionRatio.Text = String.Empty;
        }
        //
        // Event handler for exit button click
        //
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
     }
}
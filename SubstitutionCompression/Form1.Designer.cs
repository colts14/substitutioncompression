﻿namespace SubstitutionCompression
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblSelectFile = new System.Windows.Forms.Label();
            this.txtFileSelected = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.btnEncrypt = new System.Windows.Forms.Button();
            this.btnDecrypt = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lsvDisplay = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCompressionRatio = new System.Windows.Forms.TextBox();
            this.txtSizeClearText = new System.Windows.Forms.TextBox();
            this.txtSizeCipheredText = new System.Windows.Forms.TextBox();
            this.lblCompressionRatio = new System.Windows.Forms.Label();
            this.lblSizeClearText = new System.Windows.Forms.Label();
            this.lblSizeCipheredText = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSelectFile
            // 
            this.lblSelectFile.AutoSize = true;
            this.lblSelectFile.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSelectFile.Location = new System.Drawing.Point(15, 12);
            this.lblSelectFile.Name = "lblSelectFile";
            this.lblSelectFile.Size = new System.Drawing.Size(197, 15);
            this.lblSelectFile.TabIndex = 0;
            this.lblSelectFile.Text = "Select the file you would like to run:";
            // 
            // txtFileSelected
            // 
            this.txtFileSelected.BackColor = System.Drawing.SystemColors.Window;
            this.txtFileSelected.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFileSelected.Location = new System.Drawing.Point(218, 9);
            this.txtFileSelected.Name = "txtFileSelected";
            this.txtFileSelected.ReadOnly = true;
            this.txtFileSelected.Size = new System.Drawing.Size(245, 21);
            this.txtFileSelected.TabIndex = 1;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrowse.Location = new System.Drawing.Point(468, 8);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // btnEncrypt
            // 
            this.btnEncrypt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEncrypt.Location = new System.Drawing.Point(545, 8);
            this.btnEncrypt.Name = "btnEncrypt";
            this.btnEncrypt.Size = new System.Drawing.Size(75, 23);
            this.btnEncrypt.TabIndex = 3;
            this.btnEncrypt.Text = "Encrypt";
            this.btnEncrypt.UseVisualStyleBackColor = true;
            this.btnEncrypt.Click += new System.EventHandler(this.btnEncrypt_Click);
            // 
            // btnDecrypt
            // 
            this.btnDecrypt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDecrypt.Location = new System.Drawing.Point(622, 8);
            this.btnDecrypt.Name = "btnDecrypt";
            this.btnDecrypt.Size = new System.Drawing.Size(75, 23);
            this.btnDecrypt.TabIndex = 4;
            this.btnDecrypt.Text = "Decrypt";
            this.btnDecrypt.UseVisualStyleBackColor = true;
            this.btnDecrypt.Click += new System.EventHandler(this.btnDecrypt_Click);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(699, 8);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 5;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lsvDisplay
            // 
            this.lsvDisplay.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lsvDisplay.Location = new System.Drawing.Point(18, 36);
            this.lsvDisplay.Name = "lsvDisplay";
            this.lsvDisplay.Size = new System.Drawing.Size(490, 500);
            this.lsvDisplay.TabIndex = 6;
            this.lsvDisplay.UseCompatibleStateImageBehavior = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtCompressionRatio);
            this.groupBox1.Controls.Add(this.txtSizeClearText);
            this.groupBox1.Controls.Add(this.txtSizeCipheredText);
            this.groupBox1.Controls.Add(this.lblCompressionRatio);
            this.groupBox1.Controls.Add(this.lblSizeClearText);
            this.groupBox1.Controls.Add(this.lblSizeCipheredText);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(531, 135);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(240, 105);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Compression Ratio";
            // 
            // txtCompressionRatio
            // 
            this.txtCompressionRatio.BackColor = System.Drawing.SystemColors.Window;
            this.txtCompressionRatio.Location = new System.Drawing.Point(130, 70);
            this.txtCompressionRatio.Name = "txtCompressionRatio";
            this.txtCompressionRatio.ReadOnly = true;
            this.txtCompressionRatio.Size = new System.Drawing.Size(90, 21);
            this.txtCompressionRatio.TabIndex = 5;
            // 
            // txtSizeClearText
            // 
            this.txtSizeClearText.BackColor = System.Drawing.SystemColors.Window;
            this.txtSizeClearText.Location = new System.Drawing.Point(130, 46);
            this.txtSizeClearText.Name = "txtSizeClearText";
            this.txtSizeClearText.ReadOnly = true;
            this.txtSizeClearText.Size = new System.Drawing.Size(90, 21);
            this.txtSizeClearText.TabIndex = 3;
            // 
            // txtSizeCipheredText
            // 
            this.txtSizeCipheredText.BackColor = System.Drawing.SystemColors.Window;
            this.txtSizeCipheredText.Location = new System.Drawing.Point(130, 22);
            this.txtSizeCipheredText.Name = "txtSizeCipheredText";
            this.txtSizeCipheredText.ReadOnly = true;
            this.txtSizeCipheredText.Size = new System.Drawing.Size(90, 21);
            this.txtSizeCipheredText.TabIndex = 1;
            // 
            // lblCompressionRatio
            // 
            this.lblCompressionRatio.AutoSize = true;
            this.lblCompressionRatio.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompressionRatio.Location = new System.Drawing.Point(9, 71);
            this.lblCompressionRatio.Name = "lblCompressionRatio";
            this.lblCompressionRatio.Size = new System.Drawing.Size(118, 15);
            this.lblCompressionRatio.TabIndex = 4;
            this.lblCompressionRatio.Text = "Compression Ratio:";
            // 
            // lblSizeClearText
            // 
            this.lblSizeClearText.AutoSize = true;
            this.lblSizeClearText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSizeClearText.Location = new System.Drawing.Point(9, 49);
            this.lblSizeClearText.Name = "lblSizeClearText";
            this.lblSizeClearText.Size = new System.Drawing.Size(90, 15);
            this.lblSizeClearText.TabIndex = 2;
            this.lblSizeClearText.Text = "Size Clear Text:";
            // 
            // lblSizeCipheredText
            // 
            this.lblSizeCipheredText.AutoSize = true;
            this.lblSizeCipheredText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSizeCipheredText.Location = new System.Drawing.Point(9, 25);
            this.lblSizeCipheredText.Name = "lblSizeCipheredText";
            this.lblSizeCipheredText.Size = new System.Drawing.Size(111, 15);
            this.lblSizeCipheredText.TabIndex = 0;
            this.lblSizeCipheredText.Text = "Size Ciphered Text:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(788, 549);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lsvDisplay);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnDecrypt);
            this.Controls.Add(this.btnEncrypt);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtFileSelected);
            this.Controls.Add(this.lblSelectFile);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Substitution Compression";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSelectFile;
        private System.Windows.Forms.TextBox txtFileSelected;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Button btnEncrypt;
        private System.Windows.Forms.Button btnDecrypt;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ListView lsvDisplay;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblCompressionRatio;
        private System.Windows.Forms.Label lblSizeClearText;
        private System.Windows.Forms.Label lblSizeCipheredText;
        private System.Windows.Forms.TextBox txtCompressionRatio;
        private System.Windows.Forms.TextBox txtSizeClearText;
        private System.Windows.Forms.TextBox txtSizeCipheredText;
    }
}

